# **GSoC 2021 Inkscape** | On-Canvas Marker Editing

**Project Overview**

- Markers are reusable reference objects that are stored inside the definitions' section of an SVG document. Markers are not rendered directly and must be referenced from a path before they can be displayed. A path can have multiple markers to dilineate its start/mid/end locations, and a marker can be referenced from multiple paths at once. Hence, each instance of a marker element can be positioned/displayed differently based on its referring path and marker location. Inkscape does not currently support any on-canvas editing operations for markers. This project introduces essential marker editing operations like on-canvas scaling, rotation, and translation to make the Inkscape marker editing experience more robust and user-friendly.

**Technical Overview**

- This feature uses Inkscape's existing Shape Editor framework to implement on-canvas marker editing operations. 
- The rendering of a marker instance is dependant on the _referring path's_ position, shape, stroke width, and other existing transformations on the path.
- The display of a marker instance is further dictated by _native marker attributes_ such as markerUnits, markerWidth, markerHeight, viewBox, preserveAspectRatio, refX, refY, orient, and other existing transformations on svg elements contained within the marker definition.
- Some of the aforementioned transformations remain static during marker editing such as the referring path's position/shape. Thus, these transformations can be calculated beforehand and passed into the ShapeEditor as an affine transformation to assist in calculating the marker editing knot locations.
- Transformations imparted by native marker attributes such as the markerWidth, markerHeight, orient, etc., on the other hand, are calculated in realtime within the Shape Editor since their values can change while the marker is being edited. 
- Markers are validated before they are passed into the Shape Editor for editing. Any missing/incompatible attributes are automatically intialized and/or adjusted as needed.

**Related Code**

- [On Canvas Marker Editing MR](https://gitlab.com/inkscape/inkscape/-/merge_requests/3420/diffs)

**User Guide**

- The marker editing buttons can be found in the _Fill & Stroke Dialogue_ in the _Stroke Style Tab_.
- A path with start, mid, or end markers should be selected. The corresponding marker editing button can then be clicked to enter _marker editing mode_.
- The diamond shaped knot located at the center of marker can be used to adjust the positioning of the marker.
- The squared shaped knots located at the corners of the marker can be used to adjust the size of the marker. The default scaling mode is set to nonuniform scaling. The control key can be used while simultaneously adjusting the marker scaling knots to enforce uniform scaling.
- The circle shaped knot located at the corner of the marker can be used to adjust the orientation of the marker around its center of rotation.

## **Completed**

- The position of the marker is set through its refX and refY attributes. The (refX, refY) point is located at the top left corner of the marker bounding box. The marker reference knot, however, is located at the center of the marker to make the end user experience as intuitive as possible.

<div align="center">
![Marker Position](Reference.png)
</div>

- The size of the marker is set through its markerWidth, markerHeight, viewBox, and preserveAspectRatio attributes. Any preserveAspectRatio value that is not equal to "none" implements uniform scaling. Users can toggle between uniform and nonuniform scaling on canvas using the control key. Nonuniform scaling can also be used to distort and reshape the marker.

<div align="center">
![Uniform Scaling](UniformScaling.png)
![Nonuniform Scaling](NonuniformScaling.png)
</div>

- The orientation of the marker is set through its orient property. Possible values for the marker orient attribute or auto/auto-start-reverse which calculate the marker angle relative to its referencing path or it can be set to an explicit angle (deg/rad).

<div align="center">
![Rotation](Rotation.png)
</div>

- There are a number of transformations that are applied to a referenced marker before it is rendered on canvas. Because of the complexity and order of these additional transformations, on canvas editing operations such as marker rotation and marker scaling are not as straight forward as they may seem. Shown below, for example, is some unexpected behaviour a user may experience when attempting to adjust a marker's orientation through its orient property alone. The refX and refY properties of the marker remain the same but the order in which the marker transformations are applied makes it appear as though both the reference and orientation values are being updated. 
- Taking another look at xml editor in the example below, it is shown that the refX/refY values are also being updated alongside the orient property to give the illusion of a stationary reference point during the on-canvas marker rotation operations.

<div align="center">
![Rotation Adjustment](RotationAdjustment.gif)
</div>

## **Moving Forward**
- Marker rotation through its native orient property is always set to an abolute angle when it is not using auto/auto-start-reverse orient modes. This may lead to unexpected behavior for an end user who wants to adjust marker rotation but still keep its rotation relative to its referencing path. A workaround to this would be to insert a group element underneath the marker element and apply the rotation transform here while keeping the marker orient mode as auto or auto-start-reverse.
- Explore other ways to make on canvas marker editing in Inkscape more robust and as comprehensive as possible.
- Be vigilant about finding and fixing related bugs before the next release.

## **Related Links**
- https://developer.mozilla.org/en-US/docs/Web/SVG/Element/marker
- https://www.w3.org/TR/SVG11/painting.html#Markers




